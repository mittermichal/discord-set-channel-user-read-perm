simple script to enable disable discord channel read permission for specific user in specific channels

# Setup
- `pip install discord`
- `cp cfg.example.py cfg.py`
- set TOKEN in cfg.py

# Usage
python main.py true|false <target_user_id> <channel_id> <channel_id> ...
