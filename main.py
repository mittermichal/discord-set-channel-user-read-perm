import discord
import argparse
import cfg


class MyClient(discord.Client):

    def __init__(self, read_messages, user_id, channel_ids, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.read_messages = read_messages
        self.user_id = user_id
        self.channel_ids = channel_ids

    async def on_ready(self):
        print('Logged in as')
        print(self.user.name)
        print(self.user.id)
        print('------')
        print('target user id:', self.user_id)
        for channel_id in self.channel_ids:
            print('channel id:', channel_id)
            channel = self.get_channel(channel_id)
            if channel is not None:
                member = await channel.guild.fetch_member(self.user_id)
                if member is not None:
                    await channel.set_permissions(member, read_messages=self.read_messages)
                    print('permission set')
                else:
                    print('member not found')
            else:
                print("channel not found")
        await self.close()


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


parser = argparse.ArgumentParser(description='adds/removes read permission from specified user in specified channels')
parser.add_argument('read_messages', metavar='read_messages_permission', type=str2bool,
                    help='enable or disable read permission')

parser.add_argument('user_id', metavar='user_id', type=int,
                    help='Target user')

parser.add_argument('channels', metavar='channels', type=int, nargs='+',
                    help='target channels')

args = parser.parse_args()

client = MyClient(bool(args.read_messages), args.user_id, args.channels)
client.run(cfg.TOKEN)
